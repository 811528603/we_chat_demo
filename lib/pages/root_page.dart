import 'package:flutter/material.dart';
import 'package:we_chat_demo/pages/chat/chat_page.dart';
import 'package:we_chat_demo/pages/discover/discover_page.dart';
import 'package:we_chat_demo/pages/friends/friends_page.dart';
import 'package:we_chat_demo/pages/mine/mine_page.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

//icon: Icon(Icons.chat),自带的icon有选中状态不需要设置
class _RootPageState extends State<RootPage> {
  final PageController _controller = PageController(
    initialPage: 0
  );
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
            //点击事件
            onTap: (int index){
              print(index);
              _currentIndex = index;
              _controller.jumpToPage(index);
              //设置状态
              setState(() {});
            },
            //fixed 设置导航bar item 为固定状态  shifting 点击动画状态
            type: BottomNavigationBarType.fixed,
            fixedColor: Colors.green,
            selectedFontSize: 10.0,
            unselectedFontSize: 10.0,
            currentIndex: _currentIndex,
            items: [
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('images/tabbar_chat.png'),height: 24,),
                  activeIcon: Image(image: AssetImage('images/tabbar_chat_hl.png'),height: 24,),
                  title: Text('微信')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('images/tabbar_friends.png'),height: 24,),
                  activeIcon: Image(image: AssetImage('images/tabbar_friends_hl.png'),height: 24,),
                  title: Text('通讯录')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('images/tabbar_discover.png'),height: 24,),
                  activeIcon: Image(image: AssetImage('images/tabbar_discover_hl.png'),height: 24,),
                  title: Text('发现')
              ),
              BottomNavigationBarItem(
                icon: Image(image: AssetImage('images/tabbar_mine.png'),height: 24,),
                activeIcon: Image(image: AssetImage('images/tabbar_mine_hl.png'),height: 24,),
                title: Text('我'),
              ),
            ]
        ),
        //页面
        body: PageView(
          //左右滑动
          onPageChanged: (int index){
            _currentIndex = index;
            setState(() {});
          },
          //不允许滑动
          physics: NeverScrollableScrollPhysics(),
          controller: _controller,
          children: <Widget>[
            ChatPage(),
            FriendsPage(),
            DiscoverPage(),
            MinePage()
          ],
        ),
      ),
    );
  }
}
